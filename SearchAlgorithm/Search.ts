//Algorithms:
// Recherche linéaire d'un entier dans un tableau entier
function search(arr: number[], target: number): boolean {
    for (const num of arr) {
        if (num === target) {
            return true;
        }
    }
    return false;
}

// Cas de test : 
// const numbers = [1, 5, 9, 2, 8, 3];
// const targetNumber = 8;
// const isPresent = search(numbers, targetNumber);
// console.log(`La valeur ${targetNumber} est présente dans le tableau : ${isPresent}`);


// Recherche dichotomique d'un entier dans un tableau d'entiers de manière itérative : 
function binarySearch(arr: number[], target: number): boolean {
    let left = 0;
    let right = arr.length - 1;

    while (left <= right) {
        const mid = Math.floor((left + right) / 2);

        if (arr[mid] === target) {
            return true;
        } else if (arr[mid] < target) {
            left = mid + 1;
        } else {
            right = mid - 1;
        }
    }

    return false;
}
// Cas de test : 
// const numbers = [1, 3, 5, 7, 9, 11, 13];
// const targetNumber = 7;
// const isPresent = binarySearch(numbers, targetNumber);
// console.log(`La valeur ${targetNumber} est présente dans le tableau : ${isPresent}`);


//Recherche dichotomique récursive d'un entier dans un tableau d'entiers :
function binarySearchRecursive(arr: number[], target: number, left: number = 0, right: number = arr.length - 1): boolean {
    if (left > right) {
        return false;
    }

    const mid = Math.floor((left + right) / 2);

    if (arr[mid] === target) {
        return true;
    } else if (arr[mid] < target) {
        return binarySearchRecursive(arr, target, mid + 1, right);
    } else {
        return binarySearchRecursive(arr, target, left, mid - 1);
    }
}

//CAS de test:
// const numbers = [1, 3, 5, 7, 9, 11, 13];
// const targetNumber = 7;
// const isPresent = binarySearchRecursive(numbers, targetNumber);
// console.log(`La valeur ${targetNumber} est présente dans le tableau : ${isPresent}`);

//Quels sont les avantages et les inconvénients de chaque fonction ?
/*
    1.search : 
    Avantage : Dans le cas d'un petit tableau ou si on n'a pas besoin de trier le tableau à l'avance, la recherche linéaire est simple et suffisante.
    Inconvénient : La performance est inefficace pour les grands tableaux, car elle parcourt tous les éléments un par un.

    2.binarySearch:
    Avantage : cette fonction est très efficace pour les grands tableaux triés, car elle divise  l'interval de recherche par deux à chaque itération.
    Inconvénient : Nécessite que le tableau soit trié au préalable, sinon les résultats seront incorrect.

    3.binarySearchRecursive:
    Avantage : Meme chose que pour la fonction "binarySearch".
    Inconvénient : Meme chose que pour la fonction "binarySearch", mais en plus elle peut être moins efficace en termes de gestion de la pile d'appels récursifs pour de très grands tableaux,
                 ( grand nombre d'appels récursifs empilés les uns sur les autres ).
*/