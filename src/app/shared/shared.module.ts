import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { CardComponent } from './card/card.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';



@NgModule({
  declarations: [
    HeaderComponent,
    CardComponent,
    PageNotFoundComponent
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    HeaderComponent,
    CardComponent
  ]
})
export class SharedModule {

}
