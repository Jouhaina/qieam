import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-card',
    templateUrl: './card.component.html',
    styleUrls: ['./card.component.scss']
})
export class CardComponent {
    @Input() href!: string;
    @Input() alt!: string;
    @Input() src!: string;
    @Input() description!: string;
    @Input() isOnline!: boolean;
    @Input() isFriend!: boolean;
    @Input() isGame!: boolean;
    @Input() isFriends!: boolean;



    constructor() { }
}
