import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  logoImage = '../../../assets/icons/logo_h1_qi.svg'

  constructor(private authService: AuthService, private router: Router) { }
  logout(): void {
    this.authService.logout();
    this.router.navigate(['/signin']);
  }
}

