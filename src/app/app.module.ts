import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SigninComponent } from './features/signin/signin.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { GamesComponent } from './features/games/games.component';
import { GamesRoutingModule } from './features/games/games-routing.module';
import { SharedModule } from "./shared/shared.module";
import { MyGamesComponent } from './features/games/my-games/my-games.component';
import { StoreGamesComponent } from './features/games/store-games/store-games.component';
import { FriendsComponent } from './features/friends/friends.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { MockInterceptor } from './core/interceptors/mockInterceptor';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    GamesComponent,
    MyGamesComponent,
    StoreGamesComponent,
    FriendsComponent,
    MyGamesComponent,
    StoreGamesComponent
  ],
  bootstrap: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    GamesRoutingModule,
    SharedModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: MockInterceptor, multi: true },
  ],
})
export class AppModule {
}
