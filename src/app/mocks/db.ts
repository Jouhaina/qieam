import { Friend } from "../core/models/friends.model";
import { Game } from "../core/models/games.model";

const friends: Friend[] = [
    {
        "id": 1,
        "full_name": "Theodore Sevior",
        "is_friend": true
    },
    {
        "id": 2,
        "full_name": "Ginny Dufoure",
        "is_friend": false
    },
    {
        "id": 3,
        "full_name": "Kristin De Giorgi",
        "is_friend": false
    },
    {
        "id": 4,
        "full_name": "Bobina Tilliards",
        "is_friend": false
    },
    {
        "id": 5,
        "full_name": "Mikaela Crosham",
        "is_friend": false
    },
    {
        "id": 6,
        "full_name": "Aguie Southcoat",
        "is_friend": true
    },
    {
        "id": 7,
        "full_name": "Gerri Endrizzi",
        "is_friend": false
    },
    {
        "id": 8,
        "full_name": "Alberta Paula",
        "is_friend": false
    },
    {
        "id": 9,
        "full_name": "Lazar Soppett",
        "is_friend": false
    },
    {
        "id": 10,
        "full_name": "Nada Morican",
        "is_friend": true
    },
    {
        "id": 11,
        "full_name": "Maxwell Duchant",
        "is_friend": false
    },
    {
        "id": 12,
        "full_name": "Jacenta Danelut",
        "is_friend": false
    },
    {
        "id": 13,
        "full_name": "Eward Anthon",
        "is_friend": true
    },
    {
        "id": 14,
        "full_name": "Duffy Tocknell",
        "is_friend": true
    },
    {
        "id": 15,
        "full_name": "Shandee Arnaut",
        "is_friend": false
    },
    {
        "id": 16,
        "full_name": "Bendix McGillreich",
        "is_friend": true
    },
    {
        "id": 17,
        "full_name": "Anatol Scholling",
        "is_friend": true
    },
    {
        "id": 18,
        "full_name": "Katlin Biasioni",
        "is_friend": false
    },
    {
        "id": 19,
        "full_name": "Jo-anne Palluschek",
        "is_friend": true
    },
    {
        "id": 20,
        "full_name": "Rodrick Gable",
        "is_friend": true
    }
];


const games: Game[] = [
    {
        "id": "1",
        "description": "Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.\n\nPraesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.",
        "image": "https://bleedingcool.com/wp-content/uploads/2023/03/Watcher-Of-Realms-Main-Art-1200x675.jpg"
    },
    {
        "id": "2",
        "description": "Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.\n\nAenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.",
        "image": "https://files.vgtimes.ru/gallery/main/195560/8715830858_watcherofrealms.jpg"
    },
    {
        "id": "3",
        "description": "Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.\n\nCum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\n\nEtiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.",
        "image": "https://cdn.mos.cms.futurecdn.net/hMAoHhvJxSoUo6GAnvd2hd.jpg"
    },
    {
        "id": "4",
        "description": "Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.\n\nCum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\n\nEtiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.",
        "image": "https://miro.medium.com/v2/resize:fit:1358/1*ElfluOvW57WLi9VraLsQ1g.jpeg"
    },
    {
        "id": "5",
        "description": "Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.\n\nPellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.\n\nCum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
        "image": "https://wallpaper-mania.com/wp-content/uploads/2018/09/High_resolution_wallpaper_background_ID_77700887887.jpg"
    },
    {
        "id": "6",
        "description": "Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.\n\nNullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.",
        "image": "https://images.alphacoders.com/246/246223.jpg"
    },
    {
        "id": "7",
        "description": "Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.",
        "image": "https://wallpapercave.com/wp/Gtv58e3.jpg"
    },
    {
        "id": "8",
        "description": "In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.\n\nMaecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.",
        "image": "https://wallpaperaccess.com/full/1669825.jpg"
    },
    {
        "id": "9",
        "description": "Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.",
        "image": "http://dummyimage.com/320x200.png/ff4444/ffffff"
    },
    {
        "id": "10",
        "description": "Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\n\nEtiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.\n\nPraesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.",
        "image": "http://dummyimage.com/320x200.png/cc0000/ffffff"
    },
    {
        "id": "11",
        "description": "In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.",
        "image": "http://dummyimage.com/320x200.png/ff4444/ffffff"
    },
    {
        "id": "12",
        "description": "Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.\n\nQuisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.",
        "image": "http://dummyimage.com/320x200.png/5fa2dd/ffffff"
    },
    {
        "id": "13",
        "description": "Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.\n\nMaecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.",
        "image": "http://dummyimage.com/320x200.png/5fa2dd/ffffff"
    },
    {
        "id": "14",
        "description": "Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.",
        "image": "http://dummyimage.com/320x200.png/cc0000/ffffff"
    },
    {
        "id": "15",
        "description": "Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.",
        "image": "http://dummyimage.com/320x200.png/5fa2dd/ffffff"
    },
    {
        "id": "16",
        "description": "Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.",
        "image": "http://dummyimage.com/320x200.png/ff4444/ffffff"
    },
    {
        "id": "17",
        "description": "Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.",
        "image": "http://dummyimage.com/320x200.png/ff4444/ffffff"
    },
    {
        "id": "18",
        "description": "Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
        "image": "http://dummyimage.com/320x200.png/dddddd/000000"
    },
    {
        "id": "19",
        "description": "In congue. Etiam justo. Etiam pretium iaculis justo.\n\nIn hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.",
        "image": "http://dummyimage.com/320x200.png/ff4444/ffffff"
    },
    {
        "id": "20",
        "description": "Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.\n\nInteger tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.\n\nPraesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.",
        "image": "http://dummyimage.com/320x200.png/5fa2dd/ffffff"
    }
];


export { friends, games };