import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { environnment } from "src/environnments/environnment";

@Injectable({
    providedIn: 'root'
})

export class RouteService {

    constructor(private route: Router) {
        this.defineLibraryGamesApi();
        this.defineFriendsApi();
        this.defineStoreGamesApi();
    }

    defineLibraryGamesApi(): string {
        return environnment.libraryGamesApi;
    }
    defineFriendsApi(): string {
        return environnment.friendsApi;;
    }
    defineStoreGamesApi(): string {
        return environnment.storeGamesApi;
    }


}
