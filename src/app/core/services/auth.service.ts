import { Injectable } from '@angular/core';
import { SHA256 } from 'crypto-js';


@Injectable({
    providedIn: 'root'
})
export class AuthService {
    private isLoggedIn = false;
    private readonly username = 'admin';
    private readonly password = 'admin';
    private authTokenKey = SHA256('qieam_auth_token').toString();

    login(username: string, password: string): boolean {
        if (username === this.username && password === this.password) {
            this.isLoggedIn = true;
            localStorage.setItem(this.authTokenKey, this.authTokenKey);
            return true;
        }
        return this.isLoggedIn;
    }

    logout(): void {
        this.isLoggedIn = false;
        localStorage.removeItem(this.authTokenKey);
    }

    isAuthenticated(): boolean {
        return this.isLoggedIn || !!localStorage.getItem(this.authTokenKey);
    }
}

