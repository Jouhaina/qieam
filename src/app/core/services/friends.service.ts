import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Friend } from "../models/friends.model";
import { Observable } from "rxjs";
import { RouteService } from "./route.service";

@Injectable({
    providedIn: 'root'
})

export class FriendsService {
    apiUrlFriends: string;

    constructor(private http: HttpClient, private route: RouteService) {
        this.apiUrlFriends = this.route.defineFriendsApi();
    }

    getFriends(): Observable<any> {
        return this.http.get<Friend[]>(this.apiUrlFriends);
    }

}

