import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Game } from "../models/games.model";
import { environnment } from "src/environnments/environnment";
import { RouteService } from "./route.service";

@Injectable({
    providedIn: 'root'
})

export class GamessService {
    apiUrlLibrary: string;
    apiUrlStore: string;

    constructor(private http: HttpClient, private route: RouteService) {
        this.apiUrlLibrary = this.route.defineLibraryGamesApi();
        this.apiUrlStore = this.route.defineStoreGamesApi();
    }

    getGames(): Observable<any> {
        return this.http.get<Game[]>(this.apiUrlLibrary);
    }
    getStoreGames(): Observable<any> {
        return this.http.get<Game[]>(this.apiUrlStore);
    }

}
