import { Injectable } from '@angular/core';
import {
    HttpEvent,
    HttpInterceptor,
    HttpHandler,
    HttpRequest,
    HttpResponse,
} from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { friends, games } from '../../mocks/db';
import { RouteService } from '../services/route.service';

@Injectable()
export class MockInterceptor implements HttpInterceptor {
    apiUrlFriends: string;
    apiUrlLibrary: string;
    apiUrlStore: string;

    constructor(private route: RouteService) {
        this.apiUrlFriends = this.route.defineFriendsApi();
        this.apiUrlLibrary = this.route.defineLibraryGamesApi();
        this.apiUrlStore = this.route.defineStoreGamesApi();
    }

    intercept(
        request: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        const { url, method } = request;

        if (url.endsWith(this.apiUrlLibrary) && method === 'GET') {

            return of(new HttpResponse({ status: 200, body: games }));
        }
        else if (url.endsWith(this.apiUrlStore) && method === 'GET') {
            return of(new HttpResponse({ status: 200, body: games }));
        }
        else if (url.endsWith(this.apiUrlFriends) && method === 'GET') {
            return of(new HttpResponse({ status: 200, body: friends }));
        }
        return next.handle(request);
    }




}
