export interface Friend {
    id: number;
    full_name: string;
    is_friend: boolean
}