export interface Game {
    id: string;
    description: string;
    image: string
}