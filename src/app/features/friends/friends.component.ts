import { Component, OnChanges, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Friend } from 'src/app/core/models/friends.model';
import { FriendsService } from 'src/app/core/services/friends.service';

@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.scss']
})
export class FriendsComponent implements OnInit {
  src = "../../../assets/icons/Vectoruser.png";
  friends: Friend[] = [];
  friendsToShow: any[] = [];
  initialFriendsCount: number = 8;

  constructor(
    private friendsService: FriendsService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getFriends();
  }

  getFriends() {
    this.friendsService.getFriends().subscribe({
      next: (res) => {
        this.friends = res;
        this.showInitialFriends();
      },
      error(err) {
        console.error('Error fetching friends:', err);
      },
    }

    );
  }
  showInitialFriends() {
    this.friendsToShow = this.friends.slice(0, this.initialFriendsCount);
  }
  seeMoreFriends(): void {
    this.friendsToShow = this.friends;
  }
  searchFriends(event: Event): void {
    const searchText = (event.target as HTMLInputElement).value;
    if (searchText.trim() === '') {
      this.friendsToShow = this.friends.slice();
    } else {
      this.friendsToShow = this.friends.filter((friend) =>
        friend.full_name.toLowerCase().includes(searchText.toLowerCase())
      );
    }
  }
  clearSearch(): void {
    const searchInput = document.getElementById('search') as HTMLInputElement;
    this.friendsToShow = this.friends.slice();
    searchInput.value = '';
  }
  goToGames(): void {
    this.router.navigate(['/games']);
  }
}
