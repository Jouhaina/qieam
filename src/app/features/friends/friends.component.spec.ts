import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FriendsComponent } from './friends.component';
import { FriendsService } from 'src/app/core/services/friends.service';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { friends } from 'src/app/mocks/db';
import { HeaderComponent } from 'src/app/shared/header/header.component';
import { CardComponent } from 'src/app/shared/card/card.component';

describe('FriendsComponent', () => {
  let component: FriendsComponent;
  let fixture: ComponentFixture<FriendsComponent>;
  let friendsService: jasmine.SpyObj<FriendsService>;
  let router: jasmine.SpyObj<Router>;

  beforeEach(() => {
    const friendsServiceSpy = jasmine.createSpyObj('FriendsService', ['getFriends']);
    const routerSpy = jasmine.createSpyObj('Router', ['navigate']);
    TestBed.configureTestingModule({
      declarations: [FriendsComponent,
        HeaderComponent,
        CardComponent],
      providers: [
        { provide: FriendsService, useValue: friendsServiceSpy },
        { provide: Router, useValue: routerSpy },
      ]
    });
    fixture = TestBed.createComponent(FriendsComponent);
    component = fixture.componentInstance;
    friendsService = TestBed.inject(FriendsService) as jasmine.SpyObj<FriendsService>;
    router = TestBed.inject(Router) as jasmine.SpyObj<Router>;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should fetch and display initial friends', () => {
    const mockFriends: any[] = friends;

    friendsService.getFriends.and.returnValue(of(mockFriends));

    fixture.detectChanges();

    expect(friendsService.getFriends).toHaveBeenCalled();
    expect(component.friends).toEqual(mockFriends);
    expect(component.friendsToShow).toEqual(mockFriends.slice(0, component.initialFriendsCount));
  });

  it('should filter friends when searchFriends is called with a search text', () => {
    const mockFriends: any[] = friends
    component.friends = mockFriends;

    // Test with a search text that matches one friend
    component.searchFriends({ target: { value: 'Theodore' } } as unknown as Event);
    console.log(component.friendsToShow)
    expect(component.friendsToShow).toEqual([friends[0]]);

    // Test with a search text that matches multiple friends
    component.searchFriends({ target: { value: 'Ka' } } as unknown as Event);
    expect(component.friendsToShow).toEqual([friends[4], friends[17]]);

    // Test with a search text that does not match any friends
    component.searchFriends({ target: { value: 'XYZ' } } as unknown as Event);
    expect(component.friendsToShow).toEqual([]);
  });

  it('should clear search and show all friends when clearSearch is called', () => {
    const mockFriends: any[] = friends;

    component.friends = mockFriends;

    component.clearSearch();

    expect(component.friendsToShow).toEqual(mockFriends);
  });

  it('should navigate to /games when goToGames is called', () => {
    component.goToGames();

    expect(router.navigate).toHaveBeenCalledWith(['/games']);
  });
});
