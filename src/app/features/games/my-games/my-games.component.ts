import { Component } from '@angular/core';
import { Game } from 'src/app/core/models/games.model';
import { GamessService } from 'src/app/core/services/games.service';

@Component({
  selector: 'app-my-games',
  templateUrl: './my-games.component.html',
  styleUrls: ['./my-games.component.scss']
})
export class MyGamesComponent {
  games: Game[] = [];
  gamesToShow: any[] = [];
  initialGamesCount: number = 8;

  constructor(
    private gamesService: GamessService
  ) { }

  ngOnInit(): void {
    this.getGames();
  }


  getGames() {
    this.gamesService.getGames().subscribe({
      next: (res) => {
        this.games = res;
        this.showInitialGames();
      },
      error(err) {
        console.error('Error fetching games:', err);
      },
    });
  }
  showInitialGames() {
    this.gamesToShow = this.games.slice(0, this.initialGamesCount);
  }
  seeMoreGames(): void {
    this.gamesToShow = this.games;
  }
}
