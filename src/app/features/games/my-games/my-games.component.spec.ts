import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyGamesComponent } from './my-games.component';
import { GamessService } from 'src/app/core/services/games.service';
import { games } from 'src/app/mocks/db';
import { of } from 'rxjs';
import { CardComponent } from 'src/app/shared/card/card.component';

describe('MyGamesComponent', () => {
  let component: MyGamesComponent;
  let fixture: ComponentFixture<MyGamesComponent>;
  let gamesService: jasmine.SpyObj<GamessService>;

  beforeEach(() => {
    const gamesServiceSpy = jasmine.createSpyObj('GamessService', ['getGames']);
    TestBed.configureTestingModule({
      declarations: [MyGamesComponent, CardComponent],
      providers: [
        { provide: GamessService, useValue: gamesServiceSpy },
      ],
    });
    fixture = TestBed.createComponent(MyGamesComponent);
    component = fixture.componentInstance;
    gamesService = TestBed.inject(GamessService) as jasmine.SpyObj<GamessService>;

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should fetch and display initial games', () => {
    const mockGames: any[] = games;
    gamesService.getGames.and.returnValue(of(mockGames));
    fixture.detectChanges();
    expect(gamesService.getGames).toHaveBeenCalled();
    expect(component.games).toEqual(mockGames);
    expect(component.gamesToShow).toEqual(mockGames.slice(0, component.initialGamesCount));
  });

});
