import { ComponentFixture, TestBed } from '@angular/core/testing';
import { StoreGamesComponent } from './store-games.component';
import { GamessService } from 'src/app/core/services/games.service';
import { games } from 'src/app/mocks/db';
import { of } from 'rxjs';
import { CardComponent } from 'src/app/shared/card/card.component';

describe('StoreGamesComponent', () => {
  let component: StoreGamesComponent;
  let fixture: ComponentFixture<StoreGamesComponent>;
  let gamesService: jasmine.SpyObj<GamessService>;

  beforeEach(() => {
    const gamesServiceSpy = jasmine.createSpyObj('GamessService', ['getStoreGames']);
    TestBed.configureTestingModule({
      declarations: [StoreGamesComponent, CardComponent],
      providers: [
        { provide: GamessService, useValue: gamesServiceSpy },
      ],
    });
    fixture = TestBed.createComponent(StoreGamesComponent);
    component = fixture.componentInstance;
    gamesService = TestBed.inject(GamessService) as jasmine.SpyObj<GamessService>;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should fetch and display initial games', () => {
    const mockGames: any[] = games;
    gamesService.getStoreGames.and.returnValue(of(mockGames));

    fixture.detectChanges();

    expect(gamesService.getStoreGames).toHaveBeenCalled();
    expect(component.games).toEqual(mockGames);
    expect(component.gamesToShow).toEqual(mockGames.slice(0, component.initialGamesCount));
  });
});
