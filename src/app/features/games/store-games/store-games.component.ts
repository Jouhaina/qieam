import { Component } from '@angular/core';
import { Game } from 'src/app/core/models/games.model';
import { GamessService } from 'src/app/core/services/games.service';

@Component({
  selector: 'app-store-games',
  templateUrl: './store-games.component.html',
  styleUrls: ['./store-games.component.scss']
})
export class StoreGamesComponent {
  games: Game[] = [];
  gamesToShow: any[] = [];
  initialGamesCount: number = 8;

  constructor(
    private gamesService: GamessService
  ) { }

  ngOnInit(): void {
    this.getGames();
  }

  getGames(): void {
    this.gamesService.getStoreGames().subscribe({
      next: res => {
        this.games = res;
        this.showInitialGames();
      },
      error: err => {
        console.error('Error fetching games:', err);
      },
    });
  }
  showInitialGames(): void {
    this.gamesToShow = this.games.slice(0, this.initialGamesCount);
  }
  seeMoreGames(): void {
    this.gamesToShow = this.games;
  }
}
