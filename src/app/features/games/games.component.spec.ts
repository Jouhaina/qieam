import { ComponentFixture, TestBed } from '@angular/core/testing';
import { GamesComponent } from './games.component';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';
import { HeaderComponent } from 'src/app/shared/header/header.component';
import { CardComponent } from 'src/app/shared/card/card.component';
import { MyGamesComponent } from './my-games/my-games.component';
import { StoreGamesComponent } from './store-games/store-games.component';
import { HttpClientModule } from '@angular/common/http';

describe('GamesComponent', () => {
    let component: GamesComponent;
    let fixture: ComponentFixture<GamesComponent>;
    let router: Router;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [GamesComponent, HeaderComponent, CardComponent, MyGamesComponent, StoreGamesComponent],
            imports: [RouterTestingModule, HttpClientModule],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(GamesComponent);
        component = fixture.componentInstance;
        router = TestBed.inject(Router);
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should navigate to /friends when goToFriends is called', () => {
        const navigateSpy = spyOn(router, 'navigate');
        component.goToFriends();

        expect(navigateSpy).toHaveBeenCalledWith(['/friends']);
    });

});