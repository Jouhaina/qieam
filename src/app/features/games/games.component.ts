import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-games',
    templateUrl: './games.component.html',
    styleUrls: ['./games.component.scss']
})
export class GamesComponent {

    constructor(private router: Router) { }

    goToFriends(): void {
        this.router.navigate(['/friends']);
    }
}
