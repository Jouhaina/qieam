import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyGamesComponent } from './my-games/my-games.component';
import { StoreGamesComponent } from './store-games/store-games.component';

const routes: Routes = [
    { path: '', component: MyGamesComponent },
    { path: '', component: StoreGamesComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class GamesRoutingModule { }
