import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {
  logoImage = '../../../assets/icons/logo_qi.svg'
  loginForm!: FormGroup;
  loginError: boolean = false;
  invalidError: boolean = false;

  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  onSubmit(): void {
    if (this.loginForm.valid) {
      const { username, password } = this.loginForm.value;
      const isLoggedIn = this.authService.login(username, password);

      if (isLoggedIn) {
        this.router.navigate(['/games']);
      } else {
        this.loginError = true;
      }
    } else {
      this.loginError = false;
      this.invalidError = true;
    }
  }

}
