import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SigninComponent } from './signin.component';
import { AuthService } from 'src/app/core/services/auth.service';
import { Router } from '@angular/router';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';

describe('SigninComponent', () => {
  let component: SigninComponent;
  let fixture: ComponentFixture<SigninComponent>;
  let authService: jasmine.SpyObj<AuthService>;
  let router: jasmine.SpyObj<Router>;

  beforeEach(() => {
    const authServiceSpy = jasmine.createSpyObj('AuthService', ['login']);
    const routerSpy = jasmine.createSpyObj('Router', ['navigate']);

    TestBed.configureTestingModule({
      declarations: [SigninComponent],
      imports: [ReactiveFormsModule],
      providers: [
        FormBuilder,
        { provide: AuthService, useValue: authServiceSpy },
        { provide: Router, useValue: routerSpy }
      ]
    });
    fixture = TestBed.createComponent(SigninComponent);
    component = fixture.componentInstance;
    authService = TestBed.inject(AuthService) as jasmine.SpyObj<AuthService>;
    router = TestBed.inject(Router) as jasmine.SpyObj<Router>;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should navigate to /games on successful login', () => {
    authService.login.and.returnValue(true);
    component.loginForm.setValue({ username: 'admin', password: 'admin' });
    component.onSubmit();

    expect(authService.login).toHaveBeenCalledWith('admin', 'admin');
    expect(router.navigate).toHaveBeenCalledWith(['/games']);
    expect(component.loginError).toBe(false);
    expect(component.invalidError).toBe(false);
  });

  it('should set loginError to true on unsuccessful login', () => {
    authService.login.and.returnValue(false);
    component.loginForm.setValue({ username: 'admin', password: 'admin' });
    component.onSubmit();

    expect(authService.login).toHaveBeenCalledWith('admin', 'admin');
    expect(router.navigate).not.toHaveBeenCalled();
    expect(component.loginError).toBe(true);
    expect(component.invalidError).toBe(false);
  });

  it('should set invalidError to true when form is invalid', () => {
    component.loginForm.setValue({ username: '', password: '' });
    component.onSubmit();

    expect(authService.login).not.toHaveBeenCalled();
    expect(router.navigate).not.toHaveBeenCalled();
    expect(component.loginError).toBe(false);
    expect(component.invalidError).toBe(true);
  });


});
